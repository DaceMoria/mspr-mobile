# scan_codes

Une application flutter 🌊

## Commencer

En téléchargeant le projet vous devriez pouvoir l'installer sur un téléphone grâce a android studio.

Voici deux QR Codes que vous pouvez scanner avec l'application pour avoir accès à des promos toutes plus incroyables les une que les autres !

![QR#1](https://gitlab.com/DaceMoria/mspr-mobile/-/blob/master/QR%231.PNG)  
Ce code vous renverra vers une reduction pour des t-shirts.  
 
![QR#2](https://gitlab.com/DaceMoria/mspr-mobile/-/blob/master/QR%232.PNG)  
Ce code vous renverra vers une reduction pour des chausettes bob l'éponge.  

Done with 💖 by:

Macha JUMELIN -
Leo ARTHAUD -
Nicolas ASRI -
Mathieu LEPERLIER -
Arthur DE BERRANGER
