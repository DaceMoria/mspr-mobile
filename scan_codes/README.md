# scan_codes

Une application flutter 🌊

## Commencer

En téléchargeant le projet vous devriez pouvoir l'installer sur un téléphone grâce a android studio.

Voici deux QR Codes que vous pouvez scanner avec l'application pour avoir accès à des promos toutes plus incroyables les une que les autres !

![QR#1](https://github.com/LeoArthaud/MSPR_MobilApp/blob/master/QR%231.PNG)  
![QR#2](https://github.com/LeoArthaud/MSPR_MobilApp/blob/master/QR%231.PNG)

Done with 💖 by:

Macha JUMELIN -
Leo ARTHAUD -
Nicolas ASRI -
Arthur LEPERLIER -
Arthur DE BERRANGER