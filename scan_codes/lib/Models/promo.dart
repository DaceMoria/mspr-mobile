//modèle de l'objet Promo
class Promo{

  final String documentId;
  final String text;

  Promo({ this.documentId, this.text });

}