import 'package:flutter/material.dart';

import 'dart:async';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import 'package:scan_codes/Pages/promo.dart';

// Widget "HomePage"
class HomePage extends StatelessWidget {
//  @override
//  HomePageState createState() {
//    return new HomePageState();
//  }
//}
//
  ///* code fonctionnel */
//class HomePageState extends State<HomePage> {
  String result;
  BuildContext context;

  // Scan et affichage de la string récupérée ou de l'erreur
  Future _scanQR() async {
    try {
      String qrResult = await BarcodeScanner.scan();
      //String récupérée
      result = qrResult;
      pushScan(context, result);
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        result = "La permission d'utilisation de l'appareil photo à été dénié";
      } else {
        result = "Unknown Error $ex";
      }
    } on FormatException {
      result = "Vous n'avez rien scanné :(";
    } catch (ex) {
      result = "Unknown Error $ex";
    }
  }

  // PAGE DU MENU
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
              backgroundColor: Color(0xFF1b1b1b),
              title: Align(
                alignment: Alignment.center,
                child: Text(
                  "GoStyle",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 40,
                    fontFamily: 'BebasNeue',
                  ),
                ),
              )
          ),
          body: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Text(
                  'Nos promos actuelles !',
                  style: TextStyle(
                    fontSize: 30,
                    fontFamily: 'BebasNeue',
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Image.asset(
                  'assets/images/pantalon.png',
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Image.asset(
                  'assets/images/sweat.png',
                ),
              ),
            ],
          ),
          floatingActionButton: FloatingActionButton.extended(
            backgroundColor: Color(0xFF1b1b1b),
            icon: Icon(Icons.camera_alt),
            label: Text(
              "Scanner un QR Promo",
              style: TextStyle(
                fontFamily: 'BebasNeue',
              ),
            ),
            onPressed: _scanQR,
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        )
    );
  }
}

void pushScan(BuildContext context, String keyId){

  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => SecondRoute(
      keyId : keyId,
    )),
  );
}