import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:scan_codes/Services/database.dart';
import 'package:scan_codes/Pages/promoDisplay.dart';
import 'package:scan_codes/Models/promo.dart';

//Deuxiéme Page de l'application, elle affichae la promotion scannée
class SecondRoute extends StatelessWidget {

  final String keyId;

  SecondRoute({ this.keyId });

  @override
  Widget build(BuildContext context) {
    return StreamProvider<List<Promo>>.value(
      value: DatabaseService().promos,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF1b1b1b),
          title: Text(
            "Promo !!!",
            style: TextStyle(
              fontFamily: 'BebasNeue',
            ),
          ),

        ),
        body: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new promoDisplay(
                keyId : keyId,
              )
            ],
          ),
        )
      ),
    );
  }
}