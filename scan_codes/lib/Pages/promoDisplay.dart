import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:scan_codes/Models/promo.dart';

class promoDisplay extends StatefulWidget {

  final String keyId;
  promoDisplay({ this.keyId });

  @override
  _promoDisplayState createState() => _promoDisplayState();
}

//renvoie un bout de Widget de type Texte, contenant le texte de promotion !
class _promoDisplayState extends State<promoDisplay> {

  @override
  Widget build(BuildContext context) {

    String textePromo = "Erreur :(";
    final promos = Provider.of<List<Promo>>(context);

    promos.forEach((promo) {
      if (promo.documentId == widget.keyId){
        textePromo = promo.text;
      }
    });

    //Texte renvoyé
    return Text(
      textePromo,
      style: new TextStyle(
        fontSize: 20.0,
        fontFamily: 'BebasNeue',
      ),
    );
  }
}
