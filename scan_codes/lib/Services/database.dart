import "package:cloud_firestore/cloud_firestore.dart";
import "package:scan_codes/Models/promo.dart";

class DatabaseService {

  // Reference a la collection "promos" contenue en base de donnée
  final CollectionReference promoCollection = Firestore.instance.collection('promos');

  // Liste de touts les objets promos depuis une snapshot
  List<Promo> _promoListFromSnapshot(QuerySnapshot snapshot){

    return snapshot.documents.map((doc){

      print("Id :" + doc.documentID);

      return Promo(
          documentId: doc.documentID ?? '',
          text: doc.data['text'] ?? ''
      );
    }).toList();
  }

  //get promos streams
  Stream<List<Promo>> get promos {
    return promoCollection.snapshots()
    .map(_promoListFromSnapshot);
  }
}