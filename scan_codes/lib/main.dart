//Firestore import
import 'package:cloud_firestore/cloud_firestore.dart';

// packages
import 'package:flutter/material.dart';
import 'package:scan_codes/Pages/home.dart';

void main() => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
  home: HomePage(),
));