import 'package:flutter_test/flutter_test.dart';
import 'package:scan_codes/Pages/home.dart';

void main() {
  //test au lancement de l'app que la variable resulte sois bien null
  test('result null on init home', () {
    final scan = HomePage().result;
    expect(scan, null);
  });
}
