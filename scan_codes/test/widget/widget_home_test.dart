import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:scan_codes/Pages/home.dart';

void main() {
  //Lance la page d'accueil et check le titre est bien là
  testWidgets('Find title home', (WidgetTester tester) async {
    await tester.pumpWidget(HomePage());
    expect(find.text("GoStyle"), findsOneWidget);
  });
}
